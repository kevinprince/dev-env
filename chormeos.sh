#!/bin/bash

sudo su
apt-get update
apt-get -y upgrade
apt-get -y install \
    autoconf \
    apt-transport-https \
    bison \
    build-essential \
    ca-certificates \
    curl \
    git \
    gnupg2 \
    gnome-terminal \
    libssl-dev \
    libyaml-dev \
    libreadline-dev \
    zlib1g-dev \
    libncurses5-dev \
    libffi-dev \
    libgdbm3 \
    libgdbm-dev \
    libxss1 \
    libasound2 \
    nano \
    python \
    python-dev \
    ruby \
    ruby-dev \
    software-properties-common \
    unzip \
    wget

#Install Python
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

# Install VSCode
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

apt-get update
apt-get install -y code

# Install docker
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/debian \
    $(lsb_release -cs) \
    stable"

apt-get update
apt-get install docker-ce -y
usermod -aG docker nextgengames

# Install AWS tools
pip install awscli aws-mfa

# Install Ansible
pip install ansible

# Install Hashicorp tools
wget https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_linux_amd64.zip
unzip terraform_0.11.10_linux_amd64.zip
mv terraform /usr/local/bin/
rm terraform*

wget https://releases.hashicorp.com/packer/1.2.5/packer_1.2.5_linux_amd64.zip
unzip packer_1.2.5_linux_amd64.zip
mv packer /usr/local/bin/
rm packer*

exit

# Config VSCode
code --install-extension mauve.terraform
code --install vscoss.vscode-ansible
code --install rebornix.Ruby


